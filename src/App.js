import React, {Component, Fragment} from 'react';
import nanoid from 'nanoid';

import AddTaskForm from "./Components/AddTaskForm/AddTaskForm";
import Task from "./Components/Task/Task";
import './App.css';

class App extends Component {
    state = {
        tasks: [
            {taskName: "Kill Bill", id: nanoid()},
            {taskName: "Start the tractor", id: nanoid()},
            {taskName: "Do homework on JS", id: nanoid()},
        ],
        newTask: ""
    };
    currentTask = event => {
        this.setState({newTask: event.target.value});
    };
    deleteTask = id => {
        const index = this.state.tasks.findIndex(t => t.id === id);
        const tasks = [...this.state.tasks];
        tasks.splice(index, 1);
        this.setState({tasks});
    };

    createTask = () => {
        if (this.state.newTask) {
            const tasks = [...this.state.tasks];
            const task = {
                taskName: this.state.newTask,
                id: nanoid()
            };
            tasks.push(task);
            document.getElementById("addTask").value = "";
            this.setState({tasks, newTask: ""});
        }
    };

    render() {
        return (
            <div className="App">
                <Fragment>
                    <AddTaskForm
                        onChange={this.currentTask}
                        createTask={this.createTask}
                    />
                    <ul>
                        {this.state.tasks.map(task =>
                            <Task
                                key={task.id}
                                remove={() => this.deleteTask(task.id)}
                                taskName={task.taskName}
                            />
                        )}
                    </ul>
                </Fragment>

            </div>
        );
    }
}

export default App;
