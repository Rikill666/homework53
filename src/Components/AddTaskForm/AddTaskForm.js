import React from 'react';
import "./AddTaskForm.css"
const AddTaskForm = (props) => (
    <div className="addBlock">
        <input id="addTask" type="text" onChange={props.onChange}/>
        <button className="addButton" onClick={props.createTask}>Add</button>
    </div>
);

export default AddTaskForm;