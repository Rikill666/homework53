import React from 'react';
import "./Task.css"

const Task = (props) => (
    <li>
        <p>{props.taskName}</p>
        <i onClick={props.remove} class="fas fa-trash-alt"></i>
    </li>
);

export default Task;